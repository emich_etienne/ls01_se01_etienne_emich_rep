﻿/* Operatoren.java
   Uebung zu Operatoren in Java
   @author
   @version
*/
public class Operatoren {
  public static void main(String [] args){
    /* 1. Vereinbaren Sie zwei Ganzzahlen.*/
	  short zahlx, zahly;
	  
    System.out.println("UEBUNG ZU OPERATOREN IN JAVA\n");
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */
    	zahlx = 75;
    	zahly = 23;
    	System.out.println("X ist gleich:" + zahlx);
    	System.out.println("Y ist gleich:" + zahly);
    
    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    	int ergAdd = zahlx + zahly;
    	System.out.println("X + Y=" + ergAdd);
    	
    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */
    	int ergSub = zahlx - zahly;
    	System.out.println("X - Y=" + ergSub);
    	
    	int ergMul = zahlx * zahly;
    	System.out.println("X * Y=" + ergMul);
    	
    	int ergDiv = zahlx / zahly;
    	System.out.println("X / Y=" + ergDiv);
    	
    	int ergMod = zahlx % zahly;
    	System.out.println("X % Y=" + ergMod);
    	
    /* 5. Ueberprüfen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    	if (zahlx==zahly)
    		System.out.println("X ist gleich Y");
    	else
    		System.out.println("X ist nicht gleich Y");
    		
    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
    	if (zahlx!=zahly)
    		System.out.println("X ist ungleich Y");
    	else
    		System.out.println("X ist nicht ungleich Y");
    	
    	if (zahlx<zahly)
    		System.out.println("X ist kleiner als Y");
    	else
    		System.out.println("X ist nicht kleiner als Y");
    	
    	if (zahlx>zahly)
    		System.out.println("X ist größer als Y");
    	else
    		System.out.println("X ist nicht größer als Y");
    	
    /* 7. Ueberprüfen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
        if (zahlx > 0 && zahlx < 50)
        	System.out.println("X im Intervall von 0-50!");
        else
        	System.out.println("X liegt nicht im Intervall");
        
        if (zahly > 0 && zahly < 50)
        	System.out.println("Y im Intervall von 0-50!");
        else
        	System.out.println("Y liegt nicht im Intervall");
  }//main
}// Operatoren

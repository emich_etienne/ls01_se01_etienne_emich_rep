import java.util.Scanner;

public class Funktionsloeser {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie X an:");
		double x = myScanner.nextDouble();
		double e = 2.718;
		
		funktionswert(x, e);
		
	}
	public static void funktionswert(double x, double e) 
	{			
		if(x <= 0) 
		{
			double erg = Math.pow( e,x );
			System.out.println("Ihr Wert " + erg + " ist im exponentiellen Bereich");
		}
		else
		{
			if(0 < x && x <= 3) 
			{
				double erg1 = Math.pow( x,2 );
				double erg2 = erg1 + 1;
				System.out.println("Ihr Wert " + erg2 + " ist ein quadratischen Bereich");
			}
			else	
			{
				double erg = 2 * x + 4;
				System.out.println("Ihr Wert " + erg + " ist ein liniaren Bereich");
			}				
		}								
	}
}
/*
Eine Funktion y = f(x) ist in folgenden Bereichen definiert: ...
Man lese einen Wert f�r x ein und gebe den Funktionswert 
zusammen mit der Meldung aus, in welchem Bereich sich der Wert 
befindet (konstant, linear, ...). F�r die Eulersche Zahl e 
verwenden sie eine Konstante mit dem Wert 2,718.
*/
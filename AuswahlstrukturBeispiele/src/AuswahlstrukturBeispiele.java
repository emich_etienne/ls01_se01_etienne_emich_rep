import java.util.Scanner;

public class AuswahlstrukturBeispiele {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte Ihre erste Zahl ein:");
		int z1 = myScanner.nextInt();
		
		System.out.println("Geben Sie bitte Ihre zweite Zahl ein:");
		int z2= myScanner.nextInt();		
		
		kleinereZahl(z1 , z2);
	}
	
	public static void kleinereZahl(int z1, int z2) {
		if( z1 < z2 ) {
			System.out.println("Die kleinere ist " + z1 + "!");
		}else {
			if( z1 > z2 ) {
				System.out.println("Die kleinere ist " + z2 + "!");
			}else {
				System.out.println("Ihre erste Zahl ist genauso gro� wie die zweite Zahl.");
			}
		}
	}
}

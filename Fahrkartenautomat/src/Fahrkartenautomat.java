﻿import java.util.Scanner;
//Aufgabe 6
public class Fahrkartenautomat
{
    
	public static void main(String[] args)
    {
		do 	{
			
    	double gesamtBetrag = fahrkartenbestellungErfassen("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: ","Wie viele Karten möchten Sie (1-10): ");
    	double beZahlt = fahrkartenBezahlen(gesamtBetrag);
    	fahrkartenAusgeben("\nFahrschein wird ausgegeben");
    	rueckgeldAusgeben(gesamtBetrag, beZahlt); 
			}
		while(true);
    	
    }
    
    public static double fahrkartenbestellungErfassen(String a, String b)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	int silent = 0;
    	double	gesamtBetrag;
    	double 	zuZahlenderBetrag = 0;
    	double	anzahlKarten;
    	   	
    	Scanner myScanner = new Scanner(System.in);
			do
			{
				System.out.println(a);
				System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
				System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)");
				System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");				
	
				System.out.print("\nIhre Auswahl:");
	
				int auswahl = myScanner.nextInt();
	
	
				switch (auswahl) {
				case 1:
					System.out.println("Regeltarif AB [2,90 EUR]\n");
					zuZahlenderBetrag = 2.90;
					silent = 1;
					break;
				case 2:
					System.out.println("Regeltarif AB [8,60 EUR]\n");
					zuZahlenderBetrag = 8.60;
					silent = 1;
					break;
				case 3:
					System.out.println("Regeltarif AB [23,50 EUR]\n");
					zuZahlenderBetrag = 23.50;
					silent = 1;
					break;
								}
			} while (silent == 0);
    	
	    	do
	    	{
	    	System.out.println(b);
	    	anzahlKarten = tastatur.nextDouble();	
	    	gesamtBetrag = anzahlKarten * zuZahlenderBetrag;
	    	}
	    	while(anzahlKarten < 1 || anzahlKarten > 10);
	    	
    	
    	System.out.printf("\nZu zahlender Betrag (EURO): %.2f%n" , gesamtBetrag);
    	return gesamtBetrag;
    }
    
    public static double fahrkartenBezahlen(double gesamtBetrag)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double 	eingezahlterGesamtbetrag;
    	double 	eingeworfeneMünze;
    	
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < gesamtBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f%n" , (gesamtBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag;
    }
     
    public static void fahrkartenAusgeben(String a)
    {
    	System.out.println(a);
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try 
           {
 			Thread.sleep(100);
           } 
           catch (InterruptedException e) 
           {
 			
 			e.printStackTrace();
           }
        }
        System.out.println("\n\n");
    }
     
     public static void rueckgeldAusgeben(double gesamtBetrag,double beZahlt)
    {
        
    	double rueckgabebetrag = beZahlt - gesamtBetrag;
    	
    	if(rueckgabebetrag > 0.0)
    	{
    		System.out.printf("%s %.2f %s %n", "Der Rückgabebetrag in Höhe von", rueckgabebetrag, "€");
    		System.out.println("wird in folgenden Münzen ausgezahlt:");
 	   	
    		while(rueckgabebetrag >= 2.00) // 2 EURO-Münzen
    			{
    				System.out.println("2 EURO");
    				rueckgabebetrag -= 2.00;
    			}
    		
    		while(rueckgabebetrag >= 1.00) // 1 EURO-Münzen
    			{
    				System.out.println("1 EURO");
    				rueckgabebetrag -= 1.00;
    			}
    			
    		while(rueckgabebetrag >= 0.50) // 50 CENT-Münzen
    			{
    				System.out.println("50 CENT");
    				rueckgabebetrag -= 0.50;
    			}
    			
    		while(rueckgabebetrag >= 0.20) // 20 CENT-Münzen
    			{
    				System.out.println("20 CENT");
    				rueckgabebetrag -= 0.20;
    			}
    			
    		while(rueckgabebetrag >= 0.10) // 10 CENT-Münzen
    			{
    				System.out.println("10 CENT");
    				rueckgabebetrag -= 0.10;
    			}	
    		
    		while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
    			{
    				System.out.println("5 CENT");
    				rueckgabebetrag -= 0.05;
    			} 	   	
    	} 	   	
    	
    	System.out.println("\nVergessen Sie nicht, den Fahrschein zu Entnehmen!"+
    					   "\nVergessen Sie nicht, den Fahrschein zu Entwerten!"+
    					   "\nWir wuenschen Ihnen eine schöne gute Fahrt!"+
    					   "\n_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _\n");   	
    }
} 

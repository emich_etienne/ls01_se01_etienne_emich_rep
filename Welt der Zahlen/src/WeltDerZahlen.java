/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 200000000000l;
    
    // Wie viele Einwohner hat Berlin?
    float bewohnerBerlin = 3.7f;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 7665;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int   gewichtKilogramm = 200000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    int   flaecheGroessteLand = 17098242;
    
    // Wie groß ist das kleinste Land der Erde?
    
    int   flaecheKleinsteLand = 2;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Bewohner von Berlin: " + bewohnerBerlin+"Mio Einwohner");
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Alter in Tagen: " + alterTage + " Tage");
    
    System.out.println("Gewicht in Kilogramm: " + gewichtKilogramm + "Kg");
    
    System.out.println("Fl�che des gr��ten Landes der Welt: " + flaecheGroessteLand + "Km�");
    
    System.out.println("Fl�che des kleinsten Landes der Welt: " + flaecheKleinsteLand + "Km�");
    
    
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}


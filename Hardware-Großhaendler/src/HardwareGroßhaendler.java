import java.util.Scanner;

public class HardwareGro�haendler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie den Preis Ihres Produktes an");
		double preis = myScanner.nextDouble();
		System.out.println("Bitte geben Sie den Anzahl der Produkte an");
		int anzahl = myScanner.nextInt();
		
		rechnungsbetrag(anzahl, preis);
	}
	public static void rechnungsbetrag(int anzahl, double preis) {
		
		double gespreis = (anzahl * preis);
		double stepreis = (gespreis * 0.19) + gespreis;
		
		
		if(stepreis >= 0 && stepreis <= 100) {
			double rabattpreis1 = stepreis-(stepreis * 0.1);
			
			if(anzahl >= 10) {
				System.out.printf("Der Preis der Rechnung betr�gt: %.2f�" , rabattpreis1);
			}else {
				double ngespreis = rabattpreis1 + 10 ;
				System.out.printf("Der Preis der Rechnung betr�gt: %.2f�" , ngespreis);
			}
			
		}else {
			if(stepreis >100 && stepreis <= 500) {
				 double rabattpreis2 = stepreis-(stepreis * 0.15);
				 
				 if(anzahl >= 10) {
						System.out.printf("Der Preis der Rechnung betr�gt: %.2f�" , rabattpreis2);
					}else {
						double ngespreis = rabattpreis2 + 10 ;
						System.out.printf("Der Preis der Rechnung betr�gt: %.2f�" , ngespreis);
					}
				 
			}else {
					double rabattpreis3 = stepreis-(stepreis * 0.2);
					
					if(anzahl >= 10) {
						System.out.printf("Der Preis der Rechnung betr�gt: %.2f�" , rabattpreis3);
					}else {
						double ngespreis = rabattpreis3 + 10 ;
						System.out.printf("Der Preis der Rechnung betr�gt: %.2f�" , ngespreis);
					}
				}
		}
	}
}
/*if(anzahl >= 10) {
	System.out.printf("Der Preis der Rechnung betr�gt: %.2f�" , stepreis);
}else {
	double ngespreis = stepreis + 10 ;
	System.out.printf("Der Preis der Rechnung betr�gt: %.2f�" , ngespreis);
}
/*
Ein Hardware-Gro�h�ndler m�chte den Rechnungsbetrag einer 
Bestellung f�r PC-M�use mit einem Programm ermitteln. Wenn 
der Kunde mindestens 10 M�use bestellt, erfolgt die Lieferung 
frei Haus, bei einer geringeren Bestellmenge wird eine feste 
Lieferpauschale von 10 Euro erhoben. Vom Gro�h�ndler werden 
die Anzahl der bestellten M�use und der Einzelpreis einge�geben. 
Das Programm soll den Rechnungsbetrag (incl. MwSt.) aus�geben.

Der Hardware-Gro�h�ndler f�hrt ein Rabattsystem ein: 
Liegt der Bestellwert zwischen 0 und 100 �, erh�lt der 
Kunde einen Rabatt von 10 %. Liegt der Bestellwert h�her, 
aber insgesamt nicht �ber 500 �, betr�gt der Rabatt 15 %, 
in allen anderen F�llen betr�gt der Rabatt 20 %. Nach Eingabe 
des Bestellwertes soll der erm��igte Bestellwert (incl. MwSt.) 
berechnet und ausgegeben werden.
*/
import java.util.Scanner;

public class PCHaendler {
	
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String str = myScanner.next();
		return str;
	}
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		
		int erg = myScanner.nextInt();
		return erg;
	}
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		
		double nPr = myScanner.nextDouble();
		return nPr;
	}
	
	public static double berechneGnp(double anzahl, double nPr) {
		double nettogesamtpreis = anzahl * nPr;
		return nettogesamtpreis;
		
	}
	public static double berechneGbp(double nettogesamtpreis,double mSt) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mSt / 100);
		return bruttogesamtpreis;
	}
	public static void rechnungsausgabe(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mSt) {
		
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  Artikel:%-20s  Anzahl:%6d  Preis:%10.2f� %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: Artikel:%-20s  Anzahl:%6d  Preis:%10.2f� (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mSt, "%");	
		
	}
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		// Benutzereingaben lesen
			
		String artikel = liesString("Was m�chten Sie bestellen?");
		
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
					
		double nPr = liesDouble("Geben Sie den Nettopreis ein:");
		
		double mSt = liesDouble("Geben Sie den Mehrwertsteuersatz ein:");
		
		double gNp = berechneGnp(anzahl,nPr);
		
		double gBp = berechneGbp(gNp, mSt);
		
		rechnungsausgabe(artikel, anzahl, gNp, gBp, mSt);
		
		
		// Verarbeiten
			double nettogesamtpreis = anzahl * nPr;
			double bruttogesamtpreis = nettogesamtpreis * (1 + mSt / 100);		 
	}
}

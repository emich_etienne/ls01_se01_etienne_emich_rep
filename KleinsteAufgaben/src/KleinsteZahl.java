import java.util.Scanner;

public class KleinsteZahl
{
	public static void main(String[] args)
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Wie viele Zahlen m�chten Sie ausgeben?");
		
		int length = myScanner.nextInt();
		int [] zliste = new int [length];
		
		for (int i = 0; i <= length -1; i += (1))
		{
			System.out.println(( i + 1 ) + ".Zahl");
			zliste[i] = myScanner.nextInt();
		}
		
		for (int i = 0; i <= length - 1; i ++)
		{
			System.out.println( zliste[i]+ " ");
		}
		
		if (length < 1)
		{
			System.out.println("Die Liste ist leer.");
		}				
		else
		{
			double min = zliste[0];
			for (int i = 0; i <= length -1; i += (1))
			{
				if (zliste[i] < min)
				{
					 min = zliste[i];
				}
			}
			System.out.println("Die kleinste Zahl ist " + min);
		}
	}

}

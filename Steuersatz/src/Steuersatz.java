import java.util.Scanner;

public class Steuersatz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie den Nettowert Ihres Produktes an");
		double netto = myScanner.nextDouble();
		System.out.println("Falls Sie mit erm��igten Steuersatz rechnen wollen geben Sie \"j\" ein, falls sie den Regelsteuersatz wollen geben Sie \"n\" ein:");
		String satz = myScanner.next();
		
		steuersatz(netto, satz);
	}
	public static void steuersatz(double netto, String satz) {
		
		if(satz.equals("j")) {
			double preis = netto + (netto*0.07);
			System.out.printf("Ihr Preis betr�gt: %.2f�", preis );
		}else {
			if(satz.equals("n")){
				double preis = netto + (netto*0.19);
				System.out.printf("Ihr Preis betr�gt: %.2f�", preis );
		}else {
			System.out.println("Bitte geben Sie \"j\" oder \"n\" ein!");
		}
		}
	}
}
/*	
 *  Nach der Eingabe des Nettowertes soll abgefragt werden,
 *  ob der erm��igte oder der volle Steuersatz angewendet werden soll.
 *  Der Anwender entscheidet sich �ber die Eingabe von �j� f�r den erm��igten
 *  Steuersatz und mit Eingabe von �n� f�r den vollen Steuersatz.
 *  Anschlie�end soll das Programm den korrekten Bruttobetrag auf dem Bildschirm ausgeben. 
 */

public class Ladungtest {

	public static void main(String[] args) 
	{
		Ladung l1 = new Ladung();
		
		l1.setBezeichnung("Photonentorpedo");
		l1.setMenge(12);
		
		System.out.println(l1.getBezeichnung());
		System.out.println(l1.getMenge());
		System.out.println();
		
		Ladung l2 = new Ladung("Wasser", 2800);
		
		
		System.out.println(l2.getBezeichnung());
		System.out.println(l2.getMenge());
		System.out.println();

	}

}
